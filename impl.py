import io
import os
import warnings

from PySide6 import QtGui, QtCore

from PIL import Image
from stability_sdk import client
import stability_sdk.interfaces.gooseai.generation.generation_pb2 as generation


def _convert_pil_to_pixmap(input_image: Image):
    rgb_image = input_image.convert('RGB')
    data = rgb_image.tobytes('raw', 'RGB')
    return QtGui.QPixmap.fromImage(
            QtGui.QImage(
                data,
                rgb_image.size[0],
                rgb_image.size[1],
                QtGui.QImage.Format_RGB888
            ))


def _convert_qimage_to_pil(input_image: QtGui.QImage):
    buffer = QtCore.QBuffer()
    buffer.open(QtCore.QBuffer.ReadWrite)
    input_image.save(buffer, 'PNG')
    return Image.open(io.BytesIO(buffer.data()))


def _initialize_stability_api(key: str) -> client.StabilityInference:
    return client.StabilityInference(
        key=key,
        verbose=True
    )


def _generate_image_from_prompt(prompt: str) -> Image:
    stability_api = _initialize_stability_api(os.environ['STABILITY_KEY'])
    answers = stability_api.generate(
        prompt=prompt,
        seed=34567,  # if provided, specifying a random seed makes results deterministic
        steps=30,  # defaults to 50 if not specified
    )
    for resp in answers:
        for artifact in resp.artifacts:
            if artifact.finish_reason == generation.FILTER:
                warnings.warn(
                    "Your request activated the API's safety filters and could not be processed."
                    "Please modify the prompt and try again.")
            if artifact.type == generation.ARTIFACT_IMAGE:
                img = Image.open(io.BytesIO(artifact.binary))
                return img


def _modify_image_from_prompt(prompt: str, init_image) -> Image:
    stability_api = _initialize_stability_api(os.environ['STABILITY_KEY'])
    answers = stability_api.generate(
        prompt=prompt,
        init_image=init_image,
        seed=54321,
        start_schedule=0.6,  # this controls the "strength" of the prompt relative to the init image
    )

    # iterating over the generator produces the api response
    for resp in answers:
        for artifact in resp.artifacts:
            if artifact.finish_reason == generation.FILTER:
                warnings.warn(
                    "Your request activated the API's safety filters and could not be processed."
                    "Please modify the prompt and try again.")
            if artifact.type == generation.ARTIFACT_IMAGE:
                img = Image.open(io.BytesIO(artifact.binary))
                return img
