# Dreamer

A simple PySide application for generating images from text based on the Stability SDK.

This application requires the following dependencies:

- Python 3.9.x
- Stability SDK + libmagic
- PySide6
- PIL

## Stability SDK setup

1. Go to https://beta.dreamstudio.ai and register a new account.
2. Copy your API key.
3. Create an environment variable `STABILITY_KEY` and set its value to the API key.
4. Launch the application!

## Instructions

1. Install libmagic (on macOS):
```commandline
brew install libmagic
```

2. Install Python virtual environment
```commandline
pip install virtualenv
```
3. Clone the repository and go into the directory
```commandline
git clone https://gitlab.com/mateuszwojt/dreamer.git
cd dreamer
```

4. Create a new virtual environment and install the project requirements
```commandline
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

5. Run the application
```commandline
python -m main
```
