from PySide6 import QtWidgets, QtGui, QtCore

from impl import (_generate_image_from_prompt, _modify_image_from_prompt, _convert_pil_to_pixmap,
                  _convert_qimage_to_pil)


class Worker(QtCore.QThread):
    prompt = None
    input_image = None
    output_image = None

    def __init__(self):
        QtCore.QThread.__init__(self)

    def run(self):
        if self.input_image:
            self.output_image = _modify_image_from_prompt(self.prompt, self.input_image)
        else:
            self.output_image = _generate_image_from_prompt(self.prompt)


class MainWindow(QtWidgets.QDialog):
    input_image = None

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('Dreamer')
        self.setGeometry(0, 0, 600, 680)

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setSpacing(10)
        self.setLayout(self.layout)

        self.worker = Worker()

        self.create_ui()
        self.connect_ui()

    def create_ui(self):
        self.prompt_lbl = QtWidgets.QLabel('Enter your prompt:')
        self.prompt_lbl.setAlignment(QtCore.Qt.AlignCenter)
        self.layout.addWidget(self.prompt_lbl)

        self.prompt_te = QtWidgets.QTextEdit()
        self.layout.addWidget(self.prompt_te)

        or_lbl = QtWidgets.QLabel('...or...')
        or_lbl.setAlignment(QtCore.Qt.AlignCenter)
        self.layout.addWidget(or_lbl)

        self.select_image_btn = QtWidgets.QPushButton('Select initial image...')
        self.layout.addWidget(self.select_image_btn)

        self.dream_btn = QtWidgets.QPushButton('Dream! 🎇')
        self.layout.addWidget(self.dream_btn)

        self.dream_image = QtGui.QImage(512, 512, QtGui.QImage.Format_Indexed8)
        self.dream_image.fill(QtGui.qRgb(189, 149, 39))
        self.image_lbl = QtWidgets.QLabel()
        self.image_lbl.setAlignment(QtCore.Qt.AlignCenter)
        self.image_lbl.setPixmap(QtGui.QPixmap.fromImage(self.dream_image))
        self.layout.addWidget(self.image_lbl)

        self.save_btn = QtWidgets.QPushButton('Save image')
        self.layout.addWidget(self.save_btn)

    def connect_ui(self):
        self.dream_btn.clicked.connect(self.run)
        self.select_image_btn.clicked.connect(self.select_image)
        self.save_btn.clicked.connect(self.save_image)

    def select_image(self):
        filepath, _ = QtWidgets.QFileDialog.getOpenFileName(self,
                                                            'Open Image',
                                                            filter='Image Files (*.png *.jpg *.jpeg)')
        if not filepath:
            return

        self.worker.input_image = _convert_qimage_to_pil(QtGui.QImage(filepath))

    def save_image(self):
        filepath, _ = QtWidgets.QFileDialog.getSaveFileName(self, 'Save Image',
                                                            filter='Image Files (*.png)')
        self.image_lbl.pixmap().save(filepath, 'PNG', -1)

    def run(self):
        self.worker.prompt = str(self.prompt_te.toPlainText())
        self.worker.run()
        self.image_lbl.setPixmap(_convert_pil_to_pixmap(self.worker.output_image))
